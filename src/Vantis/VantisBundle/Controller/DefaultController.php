<?php

namespace Vantis\VantisBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('VantisVantisBundle:Default:index.html.twig');
    }
}
