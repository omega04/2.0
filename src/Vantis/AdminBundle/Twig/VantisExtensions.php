<?php

namespace Vantis\AdminBundle\Twig;
use Symfony\Component\Finder\Finder as Finder;

class VantisExtensions extends \Twig_Extension
{
    
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getFunctions() {
        return array(

            'getSubCategory' => new \Twig_Function_Method($this, 'getSubCategory'),
            'getCategoriesByIdNews' => new \Twig_function_Method($this, 'getCategoriesByIdNews'),
        ); 
    }  
    
    public function getSubCategory($id)
    {
       $categoryManager = $this->container->get('category_manager');
       $cats = $categoryManager->getAllSubCategory($id);
       if(!empty($cats))
           return $cats;
       else
           return null;
    }
    
    public function getCategoriesByIdNews($idNews){
        $categoryManager = $this->container->get('category_manager');
        $c2n = $this->container->get('c2n_manager');
        $c = $c2n->getCatByIdNews($idNews);
        $array = array();
        if($c){
            foreach($c as $cat){
                
                $category = $categoryManager->getCategoryById($cat->idCat);
                $array[] = $category->categoryName;
            }
        }

        return $array;
    }
    
    

    public function getName() {
        return 'vantis_extensions';
    }
}