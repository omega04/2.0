<?php

namespace Vantis\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
class NewsCategory 
{
    
    /**
     *
     * @var type integer
     */
    public $id;
    
    /**
     *
     * @var type bollean
     */
    protected $status;
    
    /**
     *
     * @var type string
     */
    public $categoryName;
    

    /**
     *
     * @var type integer
     */
    protected $level;
    
    /**
     * @var boolean
     */
    private $onlyAdmin;
    
    /**
     *
     * @var type integer
     */
    protected $parent;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return NewsCategory
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set categoryName
     *
     * @param string $categoryName
     *
     * @return NewsCategory
     */
    public function setCategoryName($categoryName)
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    /**
     * Get categoryName
     *
     * @return string
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }


    /**
     * Set level
     *
     * @param integer $level
     *
     * @return NewsCategory
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set parent
     *
     * @param integer $parent
     *
     * @return NewsCategory
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return integer
     */
    public function getParent()
    {
        return $this->parent;
    }
 


    /**
     * Set onlyAdmin
     *
     * @param boolean $onlyAdmin
     *
     * @return NewsCategory
     */
    public function setOnlyAdmin($onlyAdmin)
    {
        $this->onlyAdmin = $onlyAdmin;

        return $this;
    }

    /**
     * Get onlyAdmin
     *
     * @return boolean
     */
    public function getOnlyAdmin()
    {
        return $this->onlyAdmin;
    }
}
