<?php

namespace Vantis\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="NewsTag")
 */
class NewsTag
{
    
    
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $idNews;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tag;
    

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return NewsTag
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idNews
     *
     * @param integer $idNews
     *
     * @return NewsTag
     */
    public function setIdNews($idNews)
    {
        $this->idNews = $idNews;

        return $this;
    }

    /**
     * Get idNews
     *
     * @return integer
     */
    public function getIdNews()
    {
        return $this->idNews;
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return NewsTag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return type string
     */
    public function getTag()
    {
        return $this->tag;
    }
    
}
