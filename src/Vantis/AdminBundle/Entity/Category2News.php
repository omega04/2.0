<?php

namespace Vantis\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Category2News 
{
    /**
     *
     * @var type integer
     */
    protected $id;
    
    /**
     *
     * @var type integer
     */
    public $idNews;
    
    /**
     *
     * @var type integer
     */
    public $idCat;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idNews
     *
     * @param integer $idNews
     *
     * @return Category2News
     */
    public function setIdNews($idNews)
    {
        $this->idNews = $idNews;

        return $this;
    }

    /**
     * Get idNews
     *
     * @return integer
     */
    public function getIdNews()
    {
        return $this->idNews;
    }

    /**
     * Set idCat
     *
     * @param integer $idCat
     *
     * @return Category2News
     */
    public function setIdCat($idCat)
    {
        $this->idCat = $idCat;

        return $this;
    }

    /**
     * Get idCat
     *
     * @return integer
     */
    public function getIdCat()
    {
        return $this->idCat;
    }
}
