<?php

namespace Vantis\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class News 
{
    /**
     *
     * @var type integer
     */
    protected $id;
    
    /**
     *
     * @var type boolean
     */
    protected $aktywny;
    
 
    
    /**
     *
     * @var type date
     */
    protected $data;
    
    /**
     *
     * @var type datetime
     */
    protected $dataPublikacji;
    
    /**
     *
     * @var type string
     */
    protected $tytul;
    
    /**
     *
     * @var type text
     */
    protected $zajawka;
    
    /**
     *
     * @var type text
     */
    protected $tresc;
    
    protected $tag;
    
    
    /**
     * 
     * @return type integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 
     * @return type boolean
     */
    public function getAktywny() {
        return $this->aktywny;
    }

    /**
     * 
     * @return type date
     */
    public function getData() {
        return $this->data;
    }

    /**
     * 
     * @return type datetime
     */
    public function getDataPublikacji() {
        return $this->dataPublikacji;
    }

    /**
     * 
     * @return type integer
     */
    public function getIdKat() {
        return $this->idKat;
    }


    /**
     * 
     * @return type text
     */
    public function getTresc() {
        return $this->tresc;
    }

    /**
     * 
     * @return type
     */
    public function getTytul() {
        return $this->tytul;
    }

    /**
     * 
     * @return type text
     */
    public function getZajawka() {
        return $this->zajawka;
    }
    

    /**
     * 
     * @param type $aktywny
     * @return \Vantis\AdminBundle\Entity\News
     */
    public function setAktywny($aktywny) {
        $this->aktywny = $aktywny;
        return $this;
    }

    /**
     * 
     * @param type $data
     * @return \Vantis\AdminBundle\Entity\News
     */
    public function setData($data) {
        $this->data = $data;
        return $this;
    }

    /**
     * 
     * @param type $dataPublikacji
     * @return \Vantis\AdminBundle\Entity\News
     */
    public function setDataPublikacji($dataPublikacji) {
        $this->dataPublikacji = $dataPublikacji;
        return $this;
    }

    /**
     * 
     * @param type $idKat
     * @return \Vantis\AdminBundle\Entity\News
     */
    public function setIdKat($idKat) {
        $this->idKat = $idKat;
        return $this;
    }

    /**
     * 
     * @param type $tresc
     * @return \Vantis\AdminBundle\Entity\News
     */
    public function setTresc($tresc) {
        $this->tresc = $tresc;
        return $this;
    }

    /**
     * 
     * @param type $tytul
     * @return \Vantis\AdminBundle\Entity\News
     */
    public function setTytul($tytul) {
        $this->tytul = $tytul;
        return $this;
    }

    /**
     * 
     * @param type $zajawka
     * @return \Vantis\AdminBundle\Entity\News
     */
    public function setZajawka($zajawka) {
        $this->zajawka = $zajawka;
        return $this;
    }
    

}
