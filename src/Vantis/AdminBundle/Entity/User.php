<?php

namespace Vantis\AdminBundle\Entity;
use Symfony\Component\Security\Core\User\UserInterface;


class User implements UserInterface , \Serializable
{
    /**
     *
     * @var type integer
     */
    private $id;
    
    /**
     *
     * @var type string
     */
    private $username;
    
    /**
     *
     * @var type string
     */
    private $salt;
    
    /**
     *
     * @var type string
     */
    private $password;
    
    /**
     *
     * @var type string
     */
    private $email;
    
    /**
     *
     * @var type string
     */
    private $imie;
    
    /**
     *
     * @var type string
     */
    private $roles;
    
    /**
     *
     * @var type string
     */
    private $plainRole;
    
     /**
     *
     * @var type string
     */
    private $nazwisko;
    
    /**
     *
     * @var type datetime
     */
    private $lastLogin;
    
    public function __construct() {
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->roles = array();
    }
    /**
     * 
     * @return type string
     */
    public function getImie()
    {
        return $this->imie;
    }
    
    /**
     * 
     * @return type string
     */
    public function getNazwisko()
    {
        return $this->nazwisko;
    }
    
    /**
     * 
     * @return type string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    
    public function eraseCredentials() {
        
    }

    /**
     * 
     * @return type string
     */
    public function getPassword() {
        return $this->password;
    }

    public function getRoles() {
        return array($this->roles);
    }

    /**
     * 
     * @return type string
     */
    public function getPlainRole()
    {
        return $this->plainRole;
    }
    
    /**
     * 
     * @return type string
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * 
     * @return type string
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * 
     * @return type datetime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }
    
    
    public function serialize() {
        return serialize(array($this->id));
    }

    public function unserialize($serialized) {
        list(
             $this->id,
                ) = unserialize($serialized);
    }
    
   
    /**
     * 
     * @param type $roles
     * @return \Vantis\AdminBundle\Model\User
     */
    public function setRoles($roles)
    {
        $this->roles = (string)$roles;
        return $this;
    }
    
    /**
     * 
     * @param type $role
     * @return \Vantis\AdminBundle\Model\User
     */
    public function setPlainRole($role)
    {
        return $this->plainRole = $role;
        return $this;
    }
    
    /**
     * 
     * @param type $password
     * @return \Vantis\AdminBundle\Model\User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
    
    /**
     * 
     * @param type $email
     * @return \Vantis\AdminBundle\Model\User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    
    /**
     * 
     * @param type $imie
     * @return \Vantis\AdminBundle\Model\User
     */
    public function setImie($imie)
    {
        $this->imie = $imie;
        return $this;
    }
    
    /**
     * 
     * @param type $nazwisko
     * @return \Vantis\AdminBundle\Model\User
     */
    public function setNazwisko($nazwisko)
    {
        $this->nazwisko = $nazwisko;
        return $this;
    }
    
    
    /**
     * 
     * @param type $username
     * @return \Vantis\AdminBundle\Model\User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }
    

    public function setSalt()
    {
        return $this->salt;
    }
    
    /**
     * 
     * @param type $lastLogin
     * @return \Vantis\AdminBundle\Model\User
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }
    
    public function __toString()
    {
        return (string) $this->getUsername();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
