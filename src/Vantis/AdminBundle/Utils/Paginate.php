<?php

namespace Vantis\AdminBundle\Utils;
use Symfony\Component\HttpFoundation\Request;
class Paginate
{
    
    protected $data;
    protected $count;
    protected $offset;
    protected $left;
    protected $right;
    protected $request;
    protected $result;
    protected $curr ;
    public function __construct( $container) {
        $this->container = $container;
        $this->request =  Request::createFromGlobals();
    }


    public function Paginate($param = null ,$data, $offset = null, array $sort = null, $filterResult = null)
    {
       $this->data = $data;
       $this->offset = $offset;
       $this->param = $param;
     
       $this->result = isset($filterResult)?$filterResult:$this->data->findBy($this->param , $sort , $this->getLimit(), $this->setStart());
       return $this->result;
    }
    
    public function buildPaginator(array $param = null)
    {
       
       $extraParam = (($param) ?  $this->buildExtraParam($param) : null);
        
       for($i = 1; $i <= $this->getMax(); $i++)
       {
           $this->curr .=  (($this->getCurrent() == $i) ? $i : ''); 
          
       }
       if($this->offset == 1)
       {
            for($l = $this->getMin(); $l < ($this->curr); $l++)
            {
                $this->left .= ' <a href="?'.$extraParam.'&page='.$l.'">'.$l.'</a> ';
            }

            for($x = ($this->curr+1); $x <= $this->getMaxRange(); $x++)
            {
                $this->right .= ' <a href="?'.$extraParam.'&page='.$x.'">'.$x.'</a> ';
            }
       }else
       {
           
           for($i = $this->getMin(); $i < $this->curr; $i++)
           {
               $this->left .= ' '.(($i == $this->curr)? '' : '<a href="?'.$extraParam.'&page='.$i.'"> '.$i.'</a> ').' ';
              
           }
           
           for($l = ($this->curr+1); $l <= $this->getMaxRange(); $l++ )
           {
               $this->right .= ' '.(($this->curr == $l) ? '' : ' <a href="?'.$extraParam.'&page='.$l.'"> '.$l.'</a> ' );
           }
       }
       
        $pagin =  $this->buildPrevious($extraParam).(($this->curr >5 ) ? ' ... ' : '').$this->left.'<span class="paginatorCurrent"><b>'.$this->curr.'</span></b>'.$this->right.$this->buildRightSeparator().$this->buildNext($extraParam);
        return $pagin;
    }
    
    public function buildExtraParam($param)
    {
        $p = null;
        foreach($param as $k => $v)
        {
            $p .= '&'.$k.'='.$v;
        }
        return $p;
    }
    
    public function getMin()
    {
        if($this->curr <= 5)
        {
         $range = 1;   
        }else
        {
            $range = $this->curr-4;
        }
        return $range;
    }
    
    public function getMaxRange()
    {
        $offset = ($this->curr + 4);
        if($this->offset == 1)
        {
            $range =  (($offset < $this->countAll()) ?  $offset : $this->countAll() );
        }else
        {
             $range =  (($offset < $this->getMax()) ?  $offset : $this->getMax() );
        }
        return $range;
    }
    
    public function buildNext($param)
    {
       $next = null; 
       
       if($this->curr < round($this->getMax()))
       {
          if(empty($this->curr))
          {
             $this->curr = 1;
          }
           $next = '<a class="next" href="?'.$param.'&page='.($this->curr+1).'"> następna </a>';
           
       
       }
       return $next;
    }
    
    public function buildPrevious($param)
    {
        $previous = null;
        if($this->curr > 1)
        {
            $previous = '<a class="previous" href="?'.$param.'&page='.($this->curr-1).'"> poprzednia </a>';
        }
        return $previous;
    }
    public function getMax()
    {
        if($this->offset != 1)
        {
            $max = $this->countAll() / $this->offset;
            if($max != 1)
            {
            $max = ($max % 2 == 0 ? $max : $max+1);
            }
             return $max;
        }
        
        return $this->countAll();
       
    }
    
    public function getCurrent()
    {
       $page =  $this->request->get('page', null);
       return $page;
       
    }
    
    public function setStart()
    {
     
        return $this->getRequest(); 
    }
    
    public function getRequest()
    {
        
        $page = $this->getCurrent();
     
        if(isset($this->offset)  )
        {
            if($page == 1 || $page == null )
            {
                return 0;
            }else
                if($this->offset != 1)
                {
                    return ($page * $this->offset)- $this->offset;
                }else
                {
                    if($page == 1 || !$page)
                    {
                        return 0;
                    }else
                return $page-1;    
                }
         }
          else
            {
             return 0;
            }
    }
    
    public function countAll()
    {
         $count = count($this->data->findBy($this->param));
        
         return $count;
    }
    
    public function getLimit()
    {
        $limit = ((!$this->offset)? $this->countAll() : $this->offset); 
        return $limit;
    }
    
    public function buildRightSeparator()
    {
        $sep = null;
        if($this->offset == 1)
        {
            if($this->curr < ($this->countAll() -4))
            {
                $sep = ' ... ';
            }
        }else
        {
            if($this->curr < ($this->getMax() -4))
            {
                $sep = ' ... ';
            }
        }
        return $sep;
    }
    
}