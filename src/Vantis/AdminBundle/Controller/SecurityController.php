<?php

namespace Vantis\AdminBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
class SecurityController
{
    public function __construct($container) {
        $this->container = $container;
    }
    
    public function loginAction(Request $request)
    {
        $session = $request->getSession();
        
        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
         return $this->container->get('templating')->renderResponse('VantisAdminBundle:Login:index.html.twig', array(
            'error' => $error
        ));
    
    }
    
     public function loginCheckAction()
    {

    }
    

}