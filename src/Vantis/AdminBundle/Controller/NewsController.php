<?php

namespace Vantis\AdminBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Vantis\AdminBundle\Form\NewsFilterType;

class NewsController extends Controller
{
    public function __construct($container) {
        $this->container = $container;
    }
    
    public function indexAction(Request $request)
    {
        
        $form = $this->get('form.factory')->create(NewsFilterType::class);
        $newsManager = $this->container->get('news_manager');
        $paginator = $this->container->get('paginator');
        $tagManager = $this->container->get('tag_manager');
        $tags = $tagManager->getAllTags();
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $filterBuilder = $this->get('doctrine.orm.entity_manager')
                ->getRepository('VantisAdminBundle:News')
                ->createQueryBuilder('e');

            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);

            $query=$filterBuilder->getEntityManager();
            $q = $query->createQuery($filterBuilder->getDQL());
            
            $filterResult = $q->getResult();
            $paginate = $paginator->paginate(null, $newsManager, 2, array('dataPublikacji' => 'desc'),$filterResult);
        }else
            $paginate = $paginator->paginate(null, $newsManager, 2, array('dataPublikacji' => 'desc'),null);
  
        
        
        return $this->container->get('templating')->renderResponse('VantisAdminBundle:News:index.html.twig', array(
            'news' => $paginate,
            'paginator' => $paginator->buildPaginator(),
            'form' => $form->createView(),
            'tags' => $tags
        ));
    }
    
    
    public function editAction(Request $request)
    {
        $newsFactory = $this->container->get('news.factory');
        $newsManager = $this->container->get('news_manager');
        $tagManager = $this->container->get('tag_manager');
        $categoryManager = $this->container->get('category_manager');
        $c2n = $this->container->get('c2n_manager');
        
         if($request->isXmlHttpRequest()) {
            
             if($request->get('id')){
                 $tag= $tagManager->getTagById($request->get('id'));
                 $tagManager->deleteTag($tag);
             }
             return new JsonResponse('OK', 200);
        } else {
        $news = $newsManager->getNewsById($request->get('id'));
        $tags = $tagManager->getTagByIdNews($request->get('id'));
        $categories = $categoryManager->getAllCategory();
        $cats2News = $c2n->getCatByIdNews($request->get('id'));
        
        $form = $newsFactory->createForm();
        $form->setData($news); 
        
         if('POST' === $request->getMethod()) {
            $form->bind($request); 
            $news->setData($news->getData());
            $news->setDataPublikacji(new \DateTime($request->get('Data')));
            
            $newsManager->updateNews($news);    

            if($request->get('tag')){
            foreach($request->get('tag') as $k => $v){
                if(!$tagManager->getTagById($k))
                    $tag = $tagManager->createTag();
                else
                    $tag = $tagManager->getTagById($k);
                    
                    $tag->setIdNews($request->get('id'));
                    $tag->setTag($v);
                    $tagManager->updateTag($tag);
                
            }
            }
            foreach($cats2News as $c){
                $c2n->deleteC2N($c);
            }
            if($request->get('categories')){
            foreach($request->get('categories') as $v){
                $cat2News = $c2n->createC2N();
                $cat2News->setIdNews($request->get('id'));
                $cat2News->setIdCat($v);
                $c2n->updateC2N($cat2News);
                
            }
            }
           
            $url = $this->container->get('router')->generate('news');
            $response = new RedirectResponse($url);
            
            return $response;
            
         }   
         
        return $this->container->get('templating')->renderResponse('VantisAdminBundle:News:edit.html.twig', array(
            'form' => $form->createView(),
            'dataPublikacji' => $news->getDataPublikacji(),
            'tags' => $tags,
            'categories' => $categories,
            'c2n' => $cats2News
        )); 
        }
    }
    
    public function addAction(Request $request)
    {
        $newsFactory = $this->container->get('news.factory');
        $newsManager = $this->container->get('news_manager');
        $tagManager = $this->container->get('tag_manager');
        $categoryManager = $this->container->get('category_manager');
        $c2n = $this->container->get('c2n_manager');
        
        $form = $newsFactory->createForm();

        $categories = $categoryManager->getAllCategory();

        
        $news = $newsManager->createNews();
        $form->setData($news);
        if('POST' === $request->getMethod()) {
            $form->bind($request);
            
            
            $news->setData(new \DateTime('now'));
            $news->setDataPublikacji(new \DateTime($request->get('dataPublikacji'))); 
            $id =  $newsManager->updateNews($news);   

            
            foreach($request->get('tag') as $k => $v){
                if(!$tagManager->getTagById($k))
                    $tag = $tagManager->createTag();
                else
                    $tag = $tagManager->getTagById($k);
                    
                    $tag->setIdNews($id);
                    $tag->setTag($v);
                    $tagManager->updateTag($tag);
                
            }
            
            foreach($request->get('categories') as $v){
                $cat2News = $c2n->createC2N();
                $cat2News->setIdNews($id);
                $cat2News->setIdCat($v);
                $c2n->updateC2N($cat2News);
                
            }
            
            $url = $this->container->get('router')->generate('news');
            $response = new RedirectResponse($url);
            
            return $response;
            
        }
        
        return $this->container->get('templating')->renderResponse('VantisAdminBundle:News:add.html.twig', array(
            'form' => $form->createView(),
            'categories' => $categories,
        ));
    }
    
    
    public function deleteAction(Request $request)
    {
        $newsManager = $this->container->get('news_manager');
        
        $news = $newsManager->getNewsById($request->get('id'));
                
        $newsManager->deleteNews($news);
        
        $url = $this->container->get('router')->generate('news');
        $response = new RedirectResponse($url);
             
        return $response;
    }
   
}