<?php

namespace Vantis\AdminBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class CategoryController extends Controller
{
    public function __construct($container) {
        $this->container = $container;
    }
    
    public function indexAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $categoryManager = $this->container->get('category_manager');
        $categories = $categoryManager->getMainCategory();
        
        
        
        return $this->container->get('templating')->renderResponse('VantisAdminBundle:Category:index.html.twig', array(
        'category' => $categories
        ));
    }

     public function addAction(Request $request)
    {
         $newsFactory = $this->container->get('category.factory');
         $categoryManager = $this->container->get('category_manager');
         $form = $newsFactory->createForm();
         
         $category = $categoryManager->createCategory();
         $form->setData($category);
         if('POST' === $request->getMethod()) {
            $id = $request->get('id'); 
            $form->bind($request);
            $category->setLevel(1);
            
            $categoryManager->updateCategory($category);
            $url = $this->container->get('router')->generate('category');
            $response = new RedirectResponse($url);
            
            return $response;
         }

          return $this->container->get('templating')->renderResponse('VantisAdminBundle:Category:add.html.twig', array(
              'form' => $form->createView()
        ));
    }
    
    public function editAction(Request $request){
        
        $categoryFactory = $this->container->get('category.factory');
        $categoryFactorySubCat = $this->container->get('category.factory');
        $categoryManager = $this->container->get('category_manager');
        $categoryFactory->setName('category');
        $form = $categoryFactory->createForm();
        $subCat = $categoryFactorySubCat->createForm();
        $category = $categoryManager->getCategoryById($request->get('id'));
        $form->setData($category);
        
        $subCategories = $categoryManager->getAllSubCategory($request->get('id'));
        
        if('POST' === $request->getMethod()) {
                if($request->get('subCat')){
                    $data = $request->request->all();
                    $subCategory = $categoryManager->createCategory();
                    $subCat->bind($request);
                    $subCategory->setStatus($data['category']['status']);
                    $subCategory->setCategoryName($data['category']['categoryName']);
                    $level = $category->getLevel();
                    $subCategory->setLevel($level+1);
                    $subCategory->setParent($request->get('id'));
                    $categoryManager->updateCategory($subCategory);
                }else{
                    $form->bind($request); 
                    $categoryManager->updateCategory($category);
                }
            
        }

        return $this->container->get('templating')->renderResponse('VantisAdminBundle:Category:edit.html.twig', array(
            'form' => $form->createView(),
            'subCat' => $subCat->createView(),
            'subCategories' => $subCategories
        ));
    }
    
      public function deleteAction(Request $request)
    {
        $categoryManager = $this->container->get('category_manager');
        
        $category = $categoryManager->getCategoryById($request->get('id'));
                
        $categoryManager->deleteCategory($category);
        
        $url = $this->container->get('router')->generate('category');
        $response = new RedirectResponse($url);
             
        return $response;
    }
    
}