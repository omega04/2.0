<?php
    namespace Vantis\AdminBundle\Doctrine;
    
    use Vantis\AdminBundle\Doctrine\ORM\EntityRepository;
    use Doctrine\Common\Persistence\ObjectManager;
    
    class NewsManager extends EntityRepository
    {
        protected $class;
        protected $objectManager;
        protected $repository;
        
        public function __construct(ObjectManager $om, $class)
        {
            $this->objectManager = $om;
            $this->repository = $om->getRepository($class);
            $this->class = $class;
        }
        
        public function getClass()
        {
            return $this->class;       
        }
        
        public function createNews()
        {
            $class = $this->getClass();
            $news = new $class;
            return $news;
        }
        
        public function getAllNews()
        {
            return $this->findAll();
        }
        
        public function getNews($limit)
        {
            return $this->findBy(array('aktywny' => true), array('dataPublikacji' => 'desc'), $limit);
        }
        
        public function getNewsById($id)
        {
            return $this->find($id);
        }
        
        
      
        public function updateNews($news)
        {
            $this->objectManager->persist($news);
            $this->objectManager->flush();
            return $news->getId();
        }
        
        public function deleteNews($news)
        {
            $this->objectManager->remove($news);
            $this->objectManager->flush();
        }
        
    }