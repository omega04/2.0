<?php
    namespace Vantis\AdminBundle\Doctrine;
    
    use Vantis\AdminBundle\Doctrine\ORM\EntityRepository;
    use Doctrine\Common\Persistence\ObjectManager;
    
    class TagManager extends EntityRepository
    {
        protected $class;
        protected $objectManager;
        protected $repository;
        
        public function __construct(ObjectManager $om, $class)
        {
            $this->objectManager = $om;
            $this->repository = $om->getRepository($class);
            $this->class = $class;
        }
        
        public function getClass()
        {
            return $this->class;       
        }
        
        public function createTag()
        {
            $class = $this->getClass();
            $news = new $class;
            return $news;
        }
        
        public function getAllTags()
        {
            return $this->findAll();
        }
        
        
        public function getTagByIdNews($id)
        {
             return $this->findBy(array('idNews' => $id));
        }
        
        public function getTagById($id)
        {
            return $this->find($id);
        }
        
        public function updateTag($tag)
        {
            $this->objectManager->persist($tag);
            $this->objectManager->flush();
        }
        
        public function deleteTag($id)
        {
            $this->objectManager->remove($id);
            $this->objectManager->flush();
        }
        
    }