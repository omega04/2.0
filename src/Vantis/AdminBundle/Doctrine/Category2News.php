<?php
    namespace Vantis\AdminBundle\Doctrine;
    
    use Vantis\AdminBundle\Doctrine\ORM\EntityRepository;
    use Doctrine\Common\Persistence\ObjectManager;
    
    class Category2News extends EntityRepository
    {
        protected $class;
        protected $objectManager;
        protected $repository;
        
        public function __construct(ObjectManager $om, $class)
        {
            $this->objectManager = $om;
            $this->repository = $om->getRepository($class);
            $this->class = $class;
        }
        
        public function getClass()
        {
            return $this->class;       
        }
        
        public function createC2N()
        {
            $class = $this->getClass();
            $news = new $class;
            return $news;
        }
        
        public function getCatByIdNews($id)
        {
            return $this->findBy(array('idNews' => $id));
        }
        
        
        public function updateC2N($c2n)
        {
            $this->objectManager->persist($c2n);
            $this->objectManager->flush();
        }
        
        public function deleteC2N($c2n)
        {
            $this->objectManager->remove($c2n);
            $this->objectManager->flush();
        }
        
    }