<?php
    namespace Vantis\AdminBundle\Doctrine\ORM;
    use Doctrine\ORM\EntityRepository as BaseEntityRepository;
    use Doctrine\ORM\QueryBuilder;
    
    class EntityRepository extends BaseEntityRepository 
    {
        public function createNew() 
            {
                $className = $this->getClassMetadata();
                $class = new $className;
                return $class;
            }
            
              public function find($id)
                {
                    return $this
                        ->getQueryBuilder()
                        ->andWhere($this->getAlias().'.id = '.intval($id))
                        ->getQuery()
                        ->getOneOrNullResult()
                    ;
                }
                
             public function findAll($criteria = null, $limit = null)
                    {
                        $queryBuilder =  $this
                            ->getQueryBuilder();
                        
                        if($criteria)
                        {
                             $queryBuilder->orderBy($this->getAlias().'.'.$criteria, 'DESC');
                        }
                        if (null !== $limit) {
                            $queryBuilder->setMaxResults($limit);
                        }
                       return $queryBuilder->getQuery()
                            ->getResult()
                        ;
                        
                    }
                    
              public function findLatest($limit)
              {
                 $queryBuilder = $this->getQueryBuilder();
                 
                 $queryBuilder->setMaxResults($limit);
                 
                 $queryBuilder->orderBy($this->getAlias().'.DataPublikacji', 'DESC');
                 
                 return $queryBuilder->getQuery()->getResult();
              }
             
               public function findBySingleResult(array $criteria = null, array $order = null)
                    {
                        $queryBuilder = $this->getQueryBuilder();
                        $queryBuilder->setMaxResults(1);
                        if(null !== $criteria)
                        {
                        $this->applyCriteria($queryBuilder, $criteria);
                        }
                        
                        if((null !== $order))
                        {
                            $this->applySorting($queryBuilder, $order);
                        }
                        return $queryBuilder->getQuery()->getOneOrNullResult();
                    }
              
             public function findBy(array $criteria = null, array $orderBy = null, $limit = null, $offset = null)
                    {
                        $queryBuilder = $this->getQueryBuilder();

                        $this->applyCriteria($queryBuilder, $criteria);
                        $this->applySorting($queryBuilder, $orderBy);

                      
                        
                        if (null !== $limit) {
                            $queryBuilder->setMaxResults($limit);
                        }

                        if (null !== $offset) {
                            $queryBuilder->setFirstResult($offset);
                        }

                        return $queryBuilder
                            ->getQuery()
                            ->getResult()
                        ;
                    }
                    
             protected function applySorting(QueryBuilder $queryBuilder, array $orderBy= null)
             {
                 if(null==$orderBy)
                 {
                     return;
                 }
                 
                 foreach($orderBy as $property => $param)
                 {
                     if(!empty($param))
                     {
                            $queryBuilder->orderBy($this->getPropertyName($property), $param);
                           
                     }
                 }
             }
                    
             protected function applyCriteria(QueryBuilder $queryBuilder, array $criteria = null)
                    {
                        if (null === $criteria) {
                            return;
                        }

                        foreach ($criteria as $property => $value) {
                            if (!empty($value)) {
                                $queryBuilder
                                    ->andWhere($this->getPropertyName($property).' = :'.$property)
                                    ->setParameter($property, $value)
                                ;
                            }
                        }
                    }
                    
                    
                    
                protected function getQueryBuilder()
                    {
                        return $this->repository->createQueryBuilder($this->getAlias());
                    }
                    
              protected function getPropertyName($name)
                    {
                        if (false === strpos($name, '.')) {
                            return $this->getAlias().'.'.$name;
                        }

                        return $name;
                    }
                    
                protected function getAlias()
                    {
                        return 'o';
                    }
            
    }