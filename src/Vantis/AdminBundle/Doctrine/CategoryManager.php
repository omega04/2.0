<?php
    namespace Vantis\AdminBundle\Doctrine;
    
    use Vantis\AdminBundle\Doctrine\ORM\EntityRepository;
    use Doctrine\Common\Persistence\ObjectManager;
    
    class CategoryManager extends EntityRepository
    {
        protected $class;
        protected $objectManager;
        protected $repository;
        
        public function __construct(ObjectManager $om, $class)
        {
            $this->objectManager = $om;
            $this->repository = $om->getRepository($class);
            $this->class = $class;
        }
        
        public function getClass()
        {
            return $this->class;       
        }
        
        public function createCategory()
        {
            $class = $this->getClass();
            $news = new $class;
            return $news;
        }
        
        public function getAllCategory()
        {
            return $this->findAll();
        }
        
        public function getMainCategory($id = 1){
            return $this->findBy(array('level' => $id));
        }

        public function getAllSubCategory($id){
            return $this->findBy(array('parent' => $id));
        }
        
        public function checkChild($catId){
            return $this->findBy(array('parent' => $catId));
        }
        
        public function getCategoryById($id)
        {
            return $this->find($id);
        }
        
        
      
        public function updateCategory($news)
        {
            $this->objectManager->persist($news);
            $this->objectManager->flush();
        }
        
        public function deleteCategory($news)
        {
            $this->objectManager->remove($news);
            $this->objectManager->flush();
        }
        
    }