<?php

namespace Vantis\AdminBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class CategoryType extends AbstractType
{
    private  $class;
    
    protected $name = 'vantis_vantisbundle_category';
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
     public function __construct($class)
    {
        $this->class = $class;
    }
    
  public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           ->add('status', 'checkbox', array('label' => 'Status', 'required' => false))
           ->add('categoryName', 'text', array('label'=>'Nazwa kategorii', 'required' => true))
           ->add('onlyAdmin', 'checkbox', array('label'=>'Widoczność tylko dla Administratora', 'required' => false)); 
    }
    
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'intention' => 'category',
                
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vantis_vantisbundle_category';
    }
    
}