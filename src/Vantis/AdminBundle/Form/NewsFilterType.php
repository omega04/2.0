<?php
// ItemFilterType.php
namespace Vantis\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;

class NewsFilterType extends AbstractType
{
    protected $em;
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add('tytul', Filters\TextFilterType::class);
        $builder->add('dataPublikacji', Filters\DateRangeFilterType::class, array(
                'left_date_options' => array(
                    'widget' => 'single_text',
                    'model_timezone' => 'Europe/Paris',
                    'view_timezone' => 'Europe/Paris'
                ),
                'right_date_options' => array(
                    'widget' => 'single_text',
                    'model_timezone' => 'Europe/Paris',
                    'view_timezone' => 'Europe/Paris'
                ),
            ));
        $builder->add('tag', Filters\CollectionAdapterFilterType::class, array(
            'entry_type' => new OptionFilterType(),
            'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
                $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                $filterBuilder->leftJoin('Vantis\AdminBundle\Entity\NewsTag', $joinAlias, 'WITH',  'e.id = tag.idNews');
                };
                $qbe->addOnce($qbe->getAlias().'.tag', 'tag', $closure);
            },
        ));
            
         $builder->add('categoryName', Filters\CollectionAdapterFilterType::class, array(
            'entry_type' => new OptionCatFilterType(),
            'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
                $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                     $filterBuilder->leftJoin('Vantis\AdminBundle\Entity\Category2News', 'cat', 'WITH',  'e.id = cat.idNews');
                     $filterBuilder->leftJoin('Vantis\AdminBundle\Entity\NewsCategory', 'c', 'WITH',  'cat.idCat = c.id');
                };
                $qbe->addOnce($qbe->getAlias().'.categoryName', 'c', $closure);
            },
        ));    
            
       
        
    }

    public function getBlockPrefix()
    {
        return 'news_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering'),
            'required'               => false,
                'left_date_options'      => array(),
                'right_date_options'     => array(),
            ))
        ;
    }
}