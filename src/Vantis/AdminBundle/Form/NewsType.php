<?php

namespace Vantis\AdminBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class NewsType extends AbstractType
{
    private  $class;
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
     public function __construct($class)
    {
        $this->class = $class;
    }
    
  public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           ->add('aktywny', 'checkbox', array('label' => 'Aktywny', 'required' => false))
       
           ->add('dataPublikacji', 'text', array('label'=>'Data', 'data_class' => 'DateTime'))
           ->add('tytul', null, array('label' => 'Tytuł'))     
           ->add('zajawka', 'textarea', array('label' => 'Zajawka', 'attr' => array(
               'class' => 'tinymce',
               'data-theme' => 'advanced',
               
           )))
           ->add('tresc', 'textarea', array('label'=>'Treść', 'attr' => array(
               'class' => 'tinymce',
               'data-theme' => 'advanced',
               
           ))) 
           
        ; 
    }
    
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'intention' => 'user',
                
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vantis_vantisbundle_news';
    }
}